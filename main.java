public class main {

    public static void main(String[] args) {
        boolean [][]tab = {{false, false, true},
                            {false, true, false},
                            {false, false, true}};
        TableauStar ts = new TableauStar(tab);
        System.out.println(ts.verifieStar(2));
        System.out.println(ts.existeStar());
        System.out.println(ts.existeStarV2());
    }
}
