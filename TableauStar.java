public class TableauStar {
    private boolean [][]tab;

    public TableauStar(int n) {
        tab = new boolean[n][n];
    }

    public TableauStar(boolean [][]tab) {
        this.tab = tab;
    }

    public boolean[][] getTab() {
        return tab;
    }

    public void setTab(boolean[][] tab) {
        this.tab = tab;
    }

    // est un star si il ne s'admire que lui meme et que tout le monde l'admire
    public boolean verifieStar(int i) {
        if (!tab[i][i]) {
            return false;
        }
        for (int j = 0; j < tab.length; j++) {
            if (j != i && (!tab[j][i] || tab[i][j])) {
                return false;
            }
        }
        return true;
    }

    public boolean existeStar() {
        for (int i = 0; i < tab.length; i++) {
            if (verifieStar(i)) {
                return true;
            }
        }
        return false;
    }

    // En gros on test pour 0 si 1 l'admire, 2 l'admire etc,
    // admettons que 2 l'admire pas on sait que 0 n'est pas un star
    // on met indice star a indice de 2 et on repars jusqu'a ce qu'on arrive a la fin
    public boolean existeStarV2() {
        int indiceStar = 0;
        for (int i = 1; i < tab.length; i++) {
            if (!tab[i][indiceStar]) {
                indiceStar = i;
            }
        }
        return verifieStar(indiceStar);
    }
}
